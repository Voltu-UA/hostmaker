const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const methodOverride = require("method-override");
const path = require('path');
const config = require('./src/config');
const db = require('./src/database/db');
const property = require('./src/routes/api/property');
const index = require('./src/routes/web/index');

// Initialize DB
db.sync()
  .then(() => {
    console.log(`Connection with DB: [${ process.env.DB }] established`);
  })
  .catch(err => (console.log(`Connection to DB fails with the error: ${err}`)));

// Body parser middleware
app.set("view engine", "ejs");
app.set('views', path.join(__dirname, './src/views'));
app.use(express.static('./src/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));

// Routes middleware
app.use('/api', property);
app.use('/', index);

// Server init
app.listen(config.port, () => {
   console.log(`Server is listening on: http://localhost:${config.port}`);
});
