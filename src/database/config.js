let database;
process.env.DB && process.env.DB === 'hostmaker' ?
  database = 'hostmaker' :
  database = 'hostmaker_test';

module.exports = {
  database,
  user: 'postgres',
  password: 'postgres',
  dialect: 'postgres',
  host: 'localhost'
};
