const Sequelize = require('sequelize');
const config = require('./config');

// DB configuration
const db = new Sequelize(
  config.database,
  config.user,
  config.password,
  {
    dialect: config.dialect,
    host: config.host,
    operatorsAliases: false
  },
);

module.exports = db;
