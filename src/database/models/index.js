const Property = require('./property');
const PropertyVersion = require('./propertyVersion');

module.exports = {
  Property,
  PropertyVersion
};
