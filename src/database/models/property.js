const axios = require('axios');
const Sequelize = require('sequelize');
const db = require('../db');

// Custom validators
const validAirbnbIds = new Set();
const checkAirbnbId = async (id) => {
  if (validAirbnbIds.has(id)) { return true };
  const { status } = await axios.get(`https://www.airbnb.co.uk/rooms/${id}`);
  if (status !== 200) { return false };
  validAirbnbIds.add(id);
};

const checkRequiredAddressFields = 
  (keys, address) => keys.some((key) => typeof address[key] === 'undefined');

// const checkOptionalAddressFields = 
//   (keys, address) => keys.some((key) => address[key] === '');

// Property model
const Property = db.define('property', {
  owner: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isAlpha: true
    }
  },

  address: {
    type: Sequelize.JSON,
    allowNull: false,
    validate: {
      check: (propertyAddress) => {
        if (
          checkRequiredAddressFields(['city', 'country', 'line1', 'line4', 'postCode'], propertyAddress)
          // checkOptionalAddressFields(['line3', 'line4'], propertyAddress)
        ) {
          throw new Error('Property.address validation failed. Please check required fields');
        }
      },
      notEmpty: true
    }
  },

  numberOfBedrooms: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      min: {
        args: [0],
        msg: 'Property.numberOfBedrooms validation failed. Only values greater or equal to 0 are allowed'
      },
      notEmpty: true,
      isInt: true
    }
  },

  numberOfBathrooms: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      min: {
        args: [1],
        msg: 'Property.numberOfBathrooms validation failed. Only values greater than 0 are allowed'
      },
      notEmpty: true,
      isInt: true
    }
  },

  airbnbId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      check: (id) => checkAirbnbId(id),
      notEmpty: true,
      isInt: true
    }
  },

  incomeGenerated: {
    type: Sequelize.FLOAT,
    allowNull: false,
    validate: {
      min: {
        args: [1],
        msg: 'Property.incomeGenerated validation failed. Only values greater than 0 are allowed'
      },
      notEmpty: true,
      isFloat: true
    }
  }
});

module.exports = Property;
