const Sequelize = require('sequelize');
const db = require('../db');

// We do not need to validate the data for this model because PropertyVersion
// depends on Property model, has the same structure as Property model and
// passes through CRUD operations only when we make CRUD operations with Property model.

// PropertyVersion model
const PropertyVersion = db.define('property_version', {
  propertyId: {
    type: Sequelize.INTEGER,
  },

  owner: {
    type: Sequelize.STRING,
  },

  address: {
    type: Sequelize.JSON,
  },

  numberOfBedrooms: {
    type: Sequelize.INTEGER,
  },

  numberOfBathrooms: {
    type: Sequelize.INTEGER,
  },

  airbnbId: {
    type: Sequelize.INTEGER,
  },

  incomeGenerated: {
    type: Sequelize.FLOAT,
  }
});

module.exports = PropertyVersion;
