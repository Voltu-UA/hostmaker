const express = require('express');
const router = express.Router();
const _ = require('lodash');
const { Property, PropertyVersion } = require('../../database/models');
const db = require('../../database/db');

router.get('/', (req, res) => {
  Property
    .findAll()
    .then(properties => {
      properties.length === 0 ?
        res.status(404).end() :
        res.status(200).json(properties);
    })
    .catch(err => {
      res.status(500).json({ error: err.toString() });
    });
});

router.get('/:propertyId', (req, res) => {
  const propertyId = req.params.propertyId;
  Property
    .findById(propertyId)
    .then(property => {
      !property ?
        res.status(404).json({ error: `Property with ID: ${ propertyId } does not exist` }) :
        res.status(200).json(property);
    })
    .catch(err => {
      res.status(500).json({ error: err.toString() });
    });
});

router.get('/:propertyId/versions', (req, res) => {
  const propertyId = req.params.propertyId;
  PropertyVersion
    .findAll({ where: { propertyId }})
    .then(versions => {
      versions.length === 0 ?
        res.status(404).json({ error: `There are no available versions for property with ID: ${ propertyId }` }) :
        res.status(200).json(versions);
    })
    .catch(err => {
      res.status(500).json({ error: err.toString() });
    });
});

router.post('/', (req, res) => {
  
  // Check if request was made with form submission on the web page or
  // request was sent directly to the API in JSON format.
  if (!_.has(req.body, 'address')) {
    req.body = changeRequestData(req.body);
  }

  let createdProperty;
  db.transaction(t1 => {
    return Property
      .create(req.body, { t1 })
      .then(property => {
        createdProperty = property.get();
        const propertyId = property.get('id');
        return PropertyVersion.create(Object.assign({ propertyId }, req.body), { t1 });
      })
  })
  .then(createdPropertyVersion => {
    res.status(201).json({
      createdProperty: createdProperty,
      createdVersion: createdPropertyVersion
    });
  })
  .catch(err => {
    res.status(400).json({ error: err.toString() });
  });
});

router.put('/:propertyId', (req, res) => {

  // Check if request was made with form submission on the web page or
  // request was sent directly to the API in JSON format.
  if (!_.has(req.body, 'address')) {
    req.body = changeRequestData(req.body);
  }

  const propertyId = req.params.propertyId;
  Property
    .findById(
      req.params.propertyId,
      { attributes: { exclude: ['createdAt', 'updatedAt']}})
    .then(property => {
      if(!property) {
        return res.status(404).json({ error: `Property with ID: ${ propertyId } does not exist` });
      }
      
      property = property.get();
      
      // Add ID to req.body object
      const reqBody = Object.assign({}, req.body, { id: +propertyId });

      // Check if property object in DB alreday has the incoming updates
      const equal = _.isEqual(property, reqBody);      
      if(equal) {
        return res.status(200).json({ info: `Property with ID: ${ propertyId } is already updated` });
      }
      // Update property
      property = _.merge(property, reqBody);
      let updatedProperty; // Used in response object
      db.transaction(t1 => {
        delete property.id; // We do not need it during update operation
        return Property
          .update(property, { where: { id: propertyId }, returning: true }, { t1 })
          .then((updated) => {
            updatedProperty = _.merge({}, updated[1][0].get());
            // Build version object
            updated = updated[1][0].get();
            // We need to change id to propertyId before saving new property version
            delete updated.id;
            updated.propertyId = propertyId;
            return PropertyVersion.create(updated, { t1 });
          })
      })
      .then(updatedPropertyVersion => res.status(201).json({
        updatedProperty: updatedProperty,
        updatedVersion: updatedPropertyVersion
      }))
      .catch(err => {
        res.status(400).json({ error: err.toString() });
      });
    })
    .catch(err => {
      res.status(500).json({ error: err.toString() });
    });
});

router.delete('/:propertyId', (req, res) => {
  const propertyId = req.params.propertyId;
  let deletedProperty;
  db.transaction(t1 => {
      return Property
        .destroy({ where: { id: propertyId }, t1 })
        .then(property => {
          deletedProperty = property;
          return PropertyVersion.destroy({ where: { propertyId: propertyId }, t1 });
        });
  })
  .then(deletedVersions => {
    if(!deletedVersions) {
      return res.status(404).json({ error: `Property with ID: ${ propertyId } does not exist` });
    }
    res.status(200).json({
      propertiesDeleted: deletedProperty,
      versionsDeleted: deletedVersions
    });
  })
  .catch(err => {
    res.status(500).json({ error: err.toString() });
  });
});

// Helper function to check and transform incoming req.body object if request was made by WEB form.
const changeRequestData = (requestBody) => {
  /* 
    Incoming data from the web-form does not contain address property
    that is why we transform the req.body object so that it becomes valid
    for further interaction with the API.
  */
  
  // Gather all data except address 
  const omitAddressData = _.pick(
    requestBody,
    ['owner', 'airbnbId', 'numberOfBedrooms', 'numberOfBathrooms', 'incomeGenerated']);
  
  // Converting strings to numeric values
  const intValues = ['airbnbId', 'numberOfBedrooms', 'numberOfBathrooms'];
  for (const item of intValues) {
    omitAddressData[item] = parseInt(omitAddressData[item]);
  }
  omitAddressData.incomeGenerated = parseFloat(omitAddressData.incomeGenerated);

    // Gather address only
  const getAddressData = _.omit(
    requestBody,
    ['owner', 'airbnbId', 'numberOfBedrooms', 'numberOfBathrooms', 'incomeGenerated']);
  
  /*
    Check optional fields values.
    When request comes from web-form, the blank optional field comes as am empty string ""
    which is not convenient for futher object compartion while update operation.
  */

  if(_.has(getAddressData, 'line2') && getAddressData.line2 === '') {
    delete getAddressData.line2;
  }

  if(_.has(getAddressData, 'line3') && getAddressData.line3 === '') {
    delete getAddressData.line3;
  }

  // Build proper request object for further interaction with the API 
  requestBody = {};
  requestBody.address = getAddressData;
  requestBody = Object.assign(requestBody, omitAddressData);
  return requestBody;
};

module.exports = router;
