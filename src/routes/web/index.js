const axios = require('axios');
const express = require('express');
const router = express.Router();

const api = 'http://localhost:4000/api';

router.get('/', (req, res) => {
  return axios.get(api)
  .then(result => {
    const { data } = result;
    res.render('index', { data });
  })
  .catch(err => {
    const message = 
    'Currently there are no available properties, please add new property first!';
    res.render('new', { message });
  });
});

router.get('/new', (req, res) => {
  res.render('new', { message: '' });
});

router.get('/:propertyId', (req, res) => {
  const propertyId = req.params.propertyId;
  return axios.get(`${ api }/${ propertyId }`)
  .then(result => {
    const { data } = result;
    res.render('show', { data });
  })
  .catch(err => {
    res.render('error', { error: err.response.data.error });
  });
});

router.get('/:propertyId/versions', (req, res) => {
  const propertyId = req.params.propertyId;
  return axios.get(`${ api }/${ propertyId }/versions`)
  .then(result => {
    const { data } = result;
    res.render('versions', { data });
  })
  .catch(err => {
    res.render('error', { error: err.response.data.error });
  });
});

router.get('/:propertyId/edit', (req, res) => {
  const propertyId = req.params.propertyId;
  return axios.get(`${ api }/${ propertyId }`)
  .then(result => {
    const { data } = result;
    res.render('edit', { data });
  })
  .catch(err => {
    res.render('error', { error: err.response.data.error });
  });
});

router.post('/', (req, res) => {
  return axios.post(api, req.body)
  .then(() => {
    res.redirect('/');      
  })
  .catch(err => {
    res.render('error', { error: err.response.data.error });
  });
});

router.put('/:propertyId', (req, res) => {
  const propertyId = req.params.propertyId;
  return axios.put(`${ api }/${ propertyId }`, req.body)
  .then(() => {
    res.redirect(`/${ propertyId }`);
  })
  .catch(err => {
    res.render('error', { error: err.response.data.error });
  });
});

router.delete('/:propertyId', (req, res) => {
  const propertyId = req.params.propertyId;
  return axios.delete(`${ api }/${ propertyId }`)
  .then(() => {
    res.redirect('/');
  })
  .catch(err => {
    res.render('error', { error: err.response.data.error });
  });
});

module.exports = router;
