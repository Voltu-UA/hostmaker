const assert = require('assert');
const axios = require('axios');
const Property = require('../src/database/models/property');
const Version = require('../src/database/models/propertyVersion');
const seedData = require('../exerciseData.json');

describe('Creating Properties', () => {
  const api = 'http://localhost:4000/api'

  before(async () => {
    await Property.sync({ force:true });
    await Version.sync({ force: true });
  });

  it('Success: Create property', async () => {
    const { data, status, statusText } = await axios.post(api, seedData[0]);
    assert.equal(status, 201);
    assert.equal(statusText, 'Created');
    assert.equal(Object.keys(data).length, 2);
  });

  it('Success: Create multiple properties', async () => {
    const result = await Promise.all([
      axios.post(api, seedData[0]),
      axios.post(api, seedData[1]),
      axios.post(api, seedData[2])
    ]);
    
    assert.equal(result[0].status, 201);
    assert.equal(result[0].statusText, 'Created');
    assert.equal(result[1].status, 201);
    assert.equal(result[1].statusText, 'Created');
    assert.equal(result[2].status, 201);
    assert.equal(result[2].statusText, 'Created');
    assert.equal(result.length, 3);
  });

  it('Failure: Create property', async () => {
    
    // Prepare not valid property object
    const invalidProperty = Object.assign({}, seedData[0]);
    invalidProperty.owner = 111;
    invalidProperty.address = {};
    invalidProperty.numberOfBathrooms = '';

    await axios.post(api, invalidProperty)
      .catch((error) => {
        const { response } = error;
        assert.equal(response.status, 400);
        assert.equal(response.statusText, 'Bad Request');
        assert(response.data.error.includes('Validation error') == true);
      });
  });
});
