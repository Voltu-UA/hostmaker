const assert = require('assert');
const axios = require('axios');
const _ = require('lodash');
const Property = require('../src/database/models/property');
const Version = require('../src/database/models/propertyVersion');
const seedData = require('../exerciseData.json');

describe('Delete Properties', () => {
  const api = 'http://localhost:4000/api'

  before(async () => {
    await Property.sync({ force:true });
    await Version.sync({ force: true });

    for (const property of seedData) {
      await axios.post(api, property);
    }
  });

  it('Success: Delete property', async () => {
    const { data, status, statusText } = await axios.delete(`${ api }/1`);
    
    assert.equal(status, 200);
    assert.equal(statusText, 'OK');
    assert.equal(Object.keys(data).length, 2);
    assert.equal(data.propertiesDeleted, 1);
    assert.equal(data.versionsDeleted, 1);
  });

  it('Failure: Delete operation is not possible, property object does not exist', async () => {    
    await axios.delete(`${ api }/7`)
      .catch(error => {
        const { response } = error;

        assert.equal(response.status, 404);
        assert.equal(response.statusText, 'Not Found');
        assert.equal(response.data.error, 'Property with ID: 7 does not exist');
      });
  });
});
