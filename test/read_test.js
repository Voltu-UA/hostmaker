const assert = require('assert');
const axios = require('axios');
const _ = require('lodash');
const Property = require('../src/database/models/property');
const Version = require('../src/database/models/propertyVersion');
const seedData = require('../exerciseData.json');

describe('Read Properties', () => {
  const api = 'http://localhost:4000/api'

  before(async () => {
    await Property.sync({ force:true });
    await Version.sync({ force: true });

    for (const property of seedData) {
      await axios.post(api, property);
    }
  });

  it('Success: Get all properties', async () => {
    const { data, status, statusText } = await axios.get(api);
    assert.equal(status, 200);
    assert.equal(statusText, 'OK');
    assert.equal(data.length, 3);
  });

  it('Success: Get one property', async () => {
    const { data, status, statusText } = await axios.get(`${ api }/1`);
    
    // Object from JSON file should have this field for proper comparsion
    const property = Object.assign({}, seedData[0]);
    property.id = 1;

    // Clean unnecessary fields from response object for proper comparsion
    const responseData = _.omit(data, ['createdAt', 'updatedAt'])

    assert.equal(status, 200);
    assert.equal(statusText, 'OK');
    assert.deepStrictEqual(responseData, property);
  });

  it('Success: Get all property\'s versions', async () => {
    const { data, status, statusText } = await axios.get(`${ api }/1/versions`);
    
    // Object from JSON file should have this field for proper comparsion
    const property = Object.assign({}, seedData[0]);
    property.propertyId = 1;
    
    // Clean unnecessary fields from response object for proper comparsion
    const responseData = _.omit(data[0], ['id', 'createdAt', 'updatedAt']);
    
    assert.equal(status, 200);
    assert.equal(statusText, 'OK');
    assert.deepStrictEqual(responseData, property);
  });
});
