const assert = require('assert');
const axios = require('axios');
const _ = require('lodash');
const Property = require('../src/database/models/property');
const Version = require('../src/database/models/propertyVersion');
const seedData = require('../exerciseData.json');

describe('Update Properties', () => {
  const api = 'http://localhost:4000/api'

  before(async () => {
    await Property.sync({ force:true });
    await Version.sync({ force: true });

    for (const property of seedData) {
      await axios.post(api, property);
    }
  });

  it('Success: Update property', async () => {
    const newData = Object.assign({}, seedData[0]);
    newData.owner = 'Judy';
    newData.numberOfBedrooms = 3;
    newData.incomeGenerated = 7000.33;
    newData.id = 1; // Object from JSON file should have this field for proper comparsion

    const { data, status, statusText } = await axios.put(`${ api }/1`, newData);
    // Clean unnecessary fields from response object for proper comparsion
    let { updatedProperty } = data;
    updatedProperty = _.omit(updatedProperty, ['createdAt', 'updatedAt']);
    
    assert.equal(status, 201);
    assert.equal(statusText, 'Created');
    assert.equal(Object.keys(data).length, 2);
    assert.deepStrictEqual(newData, updatedProperty);
  });

  it('Success: Updates are not required, property object is already updated', async () => {
    const newData = Object.assign({}, seedData[1]);
    const { data, status, statusText } = await axios.put(`${ api }/2`, newData);
    
    assert.equal(status, 200);
    assert.equal(statusText, 'OK');
    assert.equal(data.info, 'Property with ID: 2 is already updated');
  });
    
  it('Failure: Updates are not possible, property object does not exist', async () => {
    const newData = Object.assign({}, seedData[0]);
    await axios.put(`${ api }/7`, newData)
      .catch(error => {
        const { response } = error;

        assert.equal(response.status, 404);
        assert.equal(response.statusText, 'Not Found');
        assert.equal(response.data.error, 'Property with ID: 7 does not exist');
    });
  });
});
